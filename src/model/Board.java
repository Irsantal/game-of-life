package model;

import java.io.Serializable;
import java.util.Random;


/**
 * The class Board represents a board of the game which is composed by Cells
 * @author Irvinn Rochette - Cyril Dubos
 */
public class Board implements Serializable{

    /**
     * The length of this Board (each Cell holds one unit)
     */
    private int length;

    /**
     * The height of this Board (each Cell holds one unit)
     */
    private int height;

    /**
     * The collection of Cell contained in ths Board
     */
    private Cell [][] cells;

    /**
     * The number of cell alive in this Board
     */
    private int population;

    /**
     * The number of current generation
     */
    private int generation;

    /**
     * creates a new board
     * @param parL the length of the board
     * @param parH the height of the board
     * @param isEmpty specify if the newest board is empty (true) or not
     */
    public Board(int parL, int parH,boolean isEmpty) {
    	length = parL;
    	height = parH;
    	cells = new Cell [length][height];
    	for (int x=0;x< length;x++) {
    		for (int y=0;y< height;y++) {
    			if(isEmpty) { //if we want a empty board
					cells[x][y]= new Cell(false);
				} else { // the case with a random board
    				Random random = new Random();
    				int number = random.nextInt(2);
					cells[x][y] = new Cell(number == 0);
				}
    		}
    	}

		countPopulation();
    }
        
    /**
     * Move to the next generation.
     * 
     * A generation is the moment where cells update their state,  depending on user's actions, and the previous generation
     */
    public void nextGeneration() {
    	boolean[][] nextStates = new boolean[length][height];

		for (int x = 0; x < length; x++) {
			for (int y = 0; y < height; y++) {
				nextStates[x][y] = cells[x][y].getState();
			}
		}

		for (int x = 0; x < length; x++) {
			for (int y = 0; y < height; y++) {
				nextStates[x][y] = nextState(x, y);
			}
		}

		for (int x = 0; x < length; x++) {
			for (int y = 0; y < height; y++) {
				cells[x][y].setState(nextStates[x][y]);
			}
		}

		generation++;
		countPopulation();
    }

    /**
     * gives the next state of the cell
     * @param column the position of the cell, following x axis
     * @param row the position of the cell, following y axis
     * @return the cell's state at the next generation
     */
	public boolean nextState(int column, int row) {
		boolean nextState = cells[column][row].getState();
		if(cells[column][row].getState() && (neighbours(column, row) < 2  || neighbours(column, row) > 3)) {
			nextState = false;
		}
		if(!cells[column][row].getState() && neighbours(column, row) == 3) {
			nextState = true;
		}

		return nextState;
	}

	/**
	 * return the number of neighbours of a cell
	 * @param column the position of a cell
	 * @param row the position of a cell
	 * @return the number of neighbours
	 */
    public int neighbours(int column, int row) {
		int neighbours = 0;

		if (column > 0) {
			if (row > 0 && cells[column - 1][row - 1].getState())
				neighbours++;
			if (cells[column - 1][row].getState())
				neighbours++;
			if (row < height - 1 && cells[column - 1][row + 1].getState())
				neighbours++;
		}

		if(row > 0 && cells[column][row - 1].getState())
			neighbours++;
		if(row < height - 1 && cells[column][row + 1].getState())
			neighbours++;

		if (column < length - 1) {
			if (row > 0 && cells[column + 1][row - 1].getState())
				neighbours++;
			if (cells[column + 1][row].getState())
				neighbours++;
			if (row < height - 1 && cells[column + 1][row + 1].getState())
				neighbours++;
		}

		return neighbours;
	}

    /**
     * count the number of cells alive
     */
	public void countPopulation() {
    	int population = 0;

		for (int i = 0; i < length; i++) {
			for (int j = 0;j < height; j++) {
				if(cells[i][j].getState()) {
					population++;
				}
			}
		}

		this.population = population;
	}

	/**
	 * Converts the board into a String objects
	 * @return a string represntation of this object
	 */
	@Override
	public String toString() {
    	String re = new String();
    	for (int i = 0; i < length; i++) {
			for (int j = 0;j < height; j++) {
				//re+=i +" "+j+" " +cells[i][j]+"\n";
				if (cells[i][j].getState()) {
					re += "X";
				} else {
					re += "O";
				}
			}
			re += "\n";
    	}
    	return re;
    }

	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @return the cells
	 */
	public Cell[][] getCells() {
		return cells;
	}

	/**
	 * @return the population
	 */
	public int getPopulation() {
		return population;
	}

	/**
	 * @return the generation
	 */
	public int getGeneration() {
		return generation;
	}
}