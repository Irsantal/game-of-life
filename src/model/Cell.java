package model;

import java.io.Serializable;

/**
 * The class Cell represents a cell of a Board which  is a structure specified by a position, and a state.
 * 
 * The position is specified by coordonates : row and column. 
 * The state is specified by a boolean : true if the cell is alive, or false if the cell is dead.
 * @author Irvinn Rochette - Cyril Dubos
 */
public class Cell implements Serializable{

    /**
     * The state of this Cell: if is alive (true) or not (false)
     */
    private boolean state;
    
    /**
     * creates a new cell
     * @param st the state of the cell : dead or alive
     */
    public Cell(boolean st) {
    	state=st;
    }
    
    /**
     * Switch the state of the cell: dead or alive
     */
    public void switchState() {
        state = !state;
    }
    
    /**
     * converts the cell into a string form
     * @return a string representation (state) of this event
     */
    public String toString() {
    	if (state)
    		return "alive";
    	else
    		return "dead";
    }

	

	/**
	 * @return the state
	 */
	public boolean getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(boolean state) {
		this.state = state;
	}

   
}