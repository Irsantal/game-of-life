package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * The class ReadWriterBoard represents a reader and a writer of Board
 * @author Irvinn Rochette - Cyril Dubos
 */
public class ReadWriteBoard {

	/**
	 * enable to open a file to read it
	 * @param parFile the file read
	 * @return a flux of the file 
	 */
	public static Object readBoard (File parFile){
		ObjectInputStream flux;
		Object readObj = null;
		
		try{
			flux= new ObjectInputStream(new FileInputStream(parFile));
			readObj=(Object)flux.readObject();
			flux.close();
		}
		catch(ClassNotFoundException parE){
			System.err.println(parE.toString());
			System.exit(1);
		}
		catch(IOException parE){
			System.err.println("Erreur lecture fichier" + parE.toString());
			System.exit(1);
		}
		return readObj;
	}//read
	
	/**
	 * enable to open a file to write it
	 * @param parFile the file read
	 * @param parObj the object added to the file
	 */
	public static void writeBoard (File parFile, Object parObj){
		ObjectOutputStream flux = null;
		//Opening write mode file
		try{
			flux= new ObjectOutputStream(new FileOutputStream(parFile));
			flux.writeObject(parObj);
			flux.flush();
			flux.close();
		}
		catch(IOException parE){
			System.err.println("Probl�me �criture\n" + parE.toString());
			System.exit(1);
		}
	}//write

}