package model;

public class Data {
	/**
	 * the name of the game
	 */
    public static String sGAME_OF_LIFE = "Jeu de la vie";
    /**
     * the message at the opening of the application
     */
    public static String sWELCOME_MESSAGE = "Ouvrez ou creez un damier...";

    /**
     * the menu item random boards
     */
    public static String sRANDOM_BOARDS = "Plateaux aleatoires";
    /**
     * the menu item saved boards
     */
    public static String sSAVED_BOARDS = "Plateaux sauvegardes";
    /**
     * the menu item empty boards
     */
    public static String sEMPTY_BOARDS = "Plateaux vides";
    /**
     * the menu refresh on the saved board
     */
    public static String sREFRESH = "Rafraichir";
    /**
     * the menu item leave
     */
    public static String sLEAVE = "Quitter";
    /**
     * the dialog box to confirm leave
     */
    public static String sCONFIRMATION = "Confirmation";

    /**
     * the label generation
     */
    public static String sGENERATION = "Generation";
    /**
     * the label population
     */
    public static String sPOPULATION = "Population";

    /**
     * the button pause
     */
    public static String sPAUSE = "Pause";
    /**
     * the button fast forward
     */
    public static String sFAST_FORWARD = "Avance rapide";
    /**
     * the button next generation
     */
    public static String sNEXT_GENERATION = "Generation suivante";
    /**
     * the label board name
     */
    public static String sBOARD_NAME = "Nom du plateau";
    /**
     * the button save
     */
    public static String sSAVE = "Sauvegarder";
    /**
     * the item edit save
     */
    public static String sEDIT_SIZE = "Personnalise";
    /**
     * the message length
     */
    public static String sLENGTH = "Longueur du plateau (entre 1 et 100)";
    /**
     * the message height
     */
    public static String sHEIGHT = "Hauteur du plateau (entre 1 et 100)";
    /**
     * the error message
     */
    public static String sERROR = "Erreur";
    /**
     * the error message size
     */
    public static String sSIZE_ERROR = "Taille du plateau invalide";
    /**
     * the character for "random" string
     */
    public static char cRANDOM = 'A';
    /**
     * the character for "saved" string
     */
    public static char cSAVED = 'S';
    /**
     * the character for "empty" string
     */
    public static char cEMPTY = 'V';
    /**
     * the character for "pause" string
     */
    public static char cPAUSE = 'P';
    /**
     * the character for "fast forward" string
     */
    public static char cFAST_FORWARD = 'R';
    /**
     * the character for "next generation" string
     */
    public static char cNEXT_GENERATION = 'G';
    /**
     * the character for "save" string
     */
    public static char cSAVE = 'U';
}
