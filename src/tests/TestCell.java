 /**
  * 
  */
package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Cell;

/**
 * Test Class for the class Cell
 * @author Irvinn Rochette - Cyril Dubos
 */
public class TestCell {

	/**
	 * Test method for {@link model.Cell#switchState()}.
	 */
	@Test
public void testSwitchState() {
		
		//first cell state is true
		Cell c = new Cell (true);
		boolean etat = c.getState();
		c.switchState();
		//assertTrue(etat!=c.getState(), "State cell before: " + etat + ", state after: "+c.getState());//JUnit5
		assertTrue("State cell before: " + etat + ", state after: "+c.getState(),etat!=c.getState());//JUnit4
		
		//first cell state is false
		c= new Cell(false);
		etat = c.getState();
		c.switchState();
		//assertTrue(etat!=c.getState(), "State cell before: " + etat + ", state after: "+c.getState());//JUnit5
		assertTrue("State cell before: " + etat + ", state after: "+c.getState(),etat!=c.getState());//JUnit4
	}

}
