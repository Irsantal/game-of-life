/**
 * 
 */
package tests;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

import model.Board;

/**
 * Test Class for the class board
 * @author Irvinn Rochette - Cyril Dubos
 */
public class TestBoard {

	/**
	 * Test method for {@link model.Board#neighbours(int, int)}.
	 */
	@Test
	public void testNeighbours() {
		Board board = new Board(10, 10, false);

        // Top / Left
        int topLeft = board.neighbours(0, 0);
        //assertTrue((topLeft >= 0) && (topLeft <=3),"Number of neighbours:" + topLeft);//JUnit 5
               
        assertTrue( "Number of neighbours:" + topLeft,(topLeft >= 0) && (topLeft <=3));//JUnit4

        // Top / Right
        int topRight = board.neighbours(board.getLength() - 1, 0);       
       // assertTrue((topRight >= 0) && (topRight <=3),"Number of neighbours:" + topLeft);//JUnit 5
        
        assertTrue( "Number of neighbours:" + topLeft,(topRight >= 0) && (topRight <=3));//JUnit4    

        // Bottom / Left
        int bottomLeft = board.neighbours(0, board.getHeight() - 1);
        //assertTrue((bottomLeft >= 0) && (bottomLeft <=3),"Number of neighbours:" + topLeft);//JUnit 5
        
        assertTrue( "Number of neighbours:" + topLeft,(bottomLeft >= 0) && (bottomLeft <=3));//JUnit4

        // Bottom / Right
        int bottomRight = board.neighbours(board.getLength() - 1, board.getHeight() - 1);
       // assertTrue((bottomRight >= 0) && (bottomRight <=3), "Number of neighbours:" + topLeft);//JUnit 5
        
        assertTrue( "Number of neighbours:" + topLeft,(bottomRight >= 0) && (bottomRight <=3));//JUnit4

        // Middle
        Random random = new Random();
        int column = random.nextInt(board.getLength());
        int row = random.nextInt(board.getHeight());
        int middle = board.neighbours(column, row);
       // assertTrue((middle >= 0) && (middle <=8), "Number of neighbours:" + topLeft);//JUnit 5
        
        assertTrue( "Number of neighbours:" + topLeft, (middle >= 0) && (middle <=8));//JUnit 4

	}
	
	/**
	 * Test method for {@link model.Board#nextState(int, int)}.
	 */
	@Test
	public void testNextState() {
		Board board = new Board(10, 10, false);
		Random random = new Random();
		int column = random.nextInt(board.getLength());
		int row = random.nextInt(board.getHeight());
		boolean state;
        int numberNeighbours = board.neighbours(column, row);
        //test if cell is alive
        board.getCells()[column][row].setState(true);
        if(numberNeighbours<2 || numberNeighbours >3) {
        	state = false;
        	//assertTrue(state==board.nextState(column, row),"Expected " + state +", obtained "+board.getCells()[row][column].getState());//JUnit5
        	
        	assertTrue("Expected " + state +", obtained "+board.getCells()[row][column].getState(),state==board.nextState(column, row));//JUnit4
        }
        //case cell is dead
        board.getCells()[column][row].setState(false);
        if(numberNeighbours==3) {
        	state = true;
        	//assertTrue(state==board.nextState(column, row),"Expected " + state +", obtained "+board.getCells()[row][column].getState());//JUnit5
        	assertTrue("Expected " + state +", obtained "+board.getCells()[row][column].getState(),state==board.nextState(column, row)); //JUnit4
        }
        
	}

}
