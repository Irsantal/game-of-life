package controller;

import view.PanelActions;
import view.PanelBoard;
import view.PanelInformations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.*;

import model.ReadWriteBoard;

/**
 * The class Controler represents the controller of the MVC
 * @author Irvinn Rochette - Cyril Dubos
 */
public class Controller extends Thread implements ActionListener {

	/**
	 * the panelInformation used in the application
	 */
	private PanelInformations panelInformations;

	/**
	 * the panelBoard used in the application
	 */
	private PanelBoard panelBoard;
		
	/**
	 * the panelActions used in the application
	 */
	private PanelActions panelActions;
	
    /**
     * creates a new controller, listening action
     * @param pi the panelInformation used in the application
     * @param pb the panelBoard used in the application
     * @param pa the panelActions used in the application
     */
    public Controller(PanelInformations pi, PanelBoard pb, PanelActions pa) {
    	panelInformations = pi;
    	panelBoard=pb;
    	panelActions=pa;
    	panelBoard.listenRecorder(this);
    	panelActions.listenRecorder(this);
    }
 
    /**
     * receive actions and realize tasks
     * @param e the event received
     */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (!(e.getActionCommand().charAt(0)=='b')) { //buttons actions command always start with a b
			StringTokenizer evt = new StringTokenizer(e.getActionCommand());
			int l = Integer.parseInt(evt.nextToken());evt.nextToken();		
			int h = Integer.parseInt(evt.nextToken());
			panelBoard.getButtonsCell()[l][h].getCell().switchState();
			panelBoard.getButtonsCell()[l][h].update();

			panelBoard.getBoard().countPopulation();

			panelBoard.update();
			panelInformations.update();
		}
		if(e.getActionCommand().equals("bNextGen")) {
			setRunning(false);//stop fast forward if launch
			panelBoard.getBoard().nextGeneration();

			panelBoard.update();
			panelInformations.update();
		}
		
		if(e.getActionCommand().equals("bFF")) {
			setRunning(true);
			
		}
		if(e.getActionCommand().contentEquals("bP")) {
			setRunning(false);
		}
		if(e.getActionCommand().contentEquals("bS")) {
			
			File f = new File("files"+File.separator +panelActions.getTextFieldName().getText()+".ser");
			ReadWriteBoard.writeBoard(f, panelBoard.getBoard());
			
			panelActions.getTextFieldName().setText("");
		}
	}

	
	//the thread
	/**
	 * specify if the thread is currently running or not
	 */
	private volatile boolean running = false;
	/**
	 * set is the thread is running or not
	 * @param running the boolean , true if the thread is running
	 */
	public void setRunning(boolean running) {
		this.running = running;
	}
	@Override
	public void run() {
		while (true) {
			if (running) {
				try {
					panelBoard.getBoard().nextGeneration();
					panelBoard.update();
					panelInformations.update();
					Thread.sleep(50);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
}