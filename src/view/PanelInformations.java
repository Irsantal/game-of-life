package view;

import model.Board;
import model.Data;

import javax.swing.*;

/**
 * The class PanelInformation represents a JPanel which contains the informations of generation and population
 * @author Irvinn Rochette - Cyril Dubos
 */
public class PanelInformations extends JPanel {

    /**
     * the game board
     */
    private Board board;

    /**
     * The JLabel that displays the number of current generation
     */
    private JLabel labelGeneration = new JLabel();

    /**
     * The JLabel that displays the number of cell alive in the Board
     */
    private JLabel labelPopulation = new JLabel();

    /**
     * displays generation and population 
     * @param board the board of the game, which contains generation and population
     */
    public PanelInformations(Board board) {
        this.board = board;

        update();

        add(labelGeneration);
        add(labelPopulation);
    }

    /**
     * updates labels with values in the board 
     */
    public void update() {
        labelGeneration.setText(Data.sGENERATION + " : " + board.getGeneration());
        labelPopulation.setText(Data.sPOPULATION + " : "+ board.getPopulation());
    }
}