package view;

import model.Board;
import model.Data;
import model.ReadWriteBoard;

import javax.swing.*;

import java.awt.Dimension;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * creates a menu bar and manages actions on this bar 
 * @author Irvinn Rochette - Cyril Dubos
 *
 */
public class MenuBar extends JMenuBar implements ActionListener {
    private Frame frame;
    private JMenu menuSaved;
    
    /**
     * creates a new menu bar for the application
     * @param frame the frame of the application
     */
    public MenuBar(Frame frame) {
        this.frame = frame;
        JMenu menuRandom = new JMenu(Data.sRANDOM_BOARDS);
        menuRandom.setMnemonic(Data.cRANDOM);

        JMenuItem itemRandom25 = new JMenuItem("25 x 25");
        itemRandom25.setActionCommand("random25");
        itemRandom25.addActionListener(this);
        itemRandom25.setAccelerator(KeyStroke.getKeyStroke('A',Event.CTRL_MASK));
        menuRandom.add(itemRandom25);

        JMenuItem itemRandom50 = new JMenuItem("50 x 50");
        itemRandom50.setActionCommand("random50");
        itemRandom50.addActionListener(this);
        itemRandom50.setAccelerator(KeyStroke.getKeyStroke('Z',Event.CTRL_MASK));
        menuRandom.add(itemRandom50);

        JMenuItem itemRandom75 = new JMenuItem("75 x 75");
        itemRandom75.setActionCommand("random75");
        itemRandom75.addActionListener(this);
        itemRandom75.setAccelerator(KeyStroke.getKeyStroke('E',Event.CTRL_MASK));
        menuRandom.add(itemRandom75);
        
        menuRandom.addSeparator();

        JMenuItem itemRandom = new JMenuItem(Data.sEDIT_SIZE);
        itemRandom.setActionCommand("random");
        itemRandom.addActionListener(this);
        itemRandom.setAccelerator(KeyStroke.getKeyStroke('R',Event.CTRL_MASK));
        menuRandom.add(itemRandom);

        add(menuRandom);


        menuSaved = new JMenu(Data.sSAVED_BOARDS);
        menuSaved.setMnemonic(Data.cSAVED);
        JMenuItem refresh = new JMenuItem(Data.sREFRESH, new ImageIcon("f5i.png"));
        refresh.setActionCommand("f5");
        refresh.addActionListener(this);
        menuSaved.addSeparator();
        menuSaved.add(refresh);
        add(menuSaved);


        JMenu menuEmpty = new JMenu(Data.sEMPTY_BOARDS);
        menuEmpty.setMnemonic(Data.cEMPTY);

        JMenuItem itemEmpty25 = new JMenuItem("25 x 25");
        itemEmpty25.setActionCommand("empty25");
        itemEmpty25.addActionListener(this);
        itemEmpty25.setAccelerator(KeyStroke.getKeyStroke('Q',Event.CTRL_MASK));
        menuEmpty.add(itemEmpty25);

        JMenuItem itemEmpty50 = new JMenuItem("50 x 50");
        itemEmpty50.setActionCommand("empty50");
        itemEmpty50.addActionListener(this);
        itemEmpty50.setAccelerator(KeyStroke.getKeyStroke('S',Event.CTRL_MASK));
        menuEmpty.add(itemEmpty50);

        JMenuItem itemEmpty75 = new JMenuItem("75 x 75");
        itemEmpty75.setActionCommand("empty75");
        itemEmpty75.addActionListener(this);
        itemEmpty75.setAccelerator(KeyStroke.getKeyStroke('D',Event.CTRL_MASK));
        menuEmpty.add(itemEmpty75);

        menuEmpty.addSeparator();
        
        JMenuItem itemEmpty = new JMenuItem(Data.sEDIT_SIZE);
        itemEmpty.setActionCommand("empty");
        itemEmpty.addActionListener(this);
        itemEmpty.setAccelerator(KeyStroke.getKeyStroke('F',Event.CTRL_MASK));
        menuEmpty.add(itemEmpty);

        add(menuEmpty);

        JMenuItem leave = new JMenuItem(Data.sLEAVE);
        leave.setActionCommand("leave");
        leave.addActionListener(this);
        leave.setMaximumSize(new Dimension(200,50));
        leave.setAccelerator(KeyStroke.getKeyStroke('L',Event.CTRL_MASK));
        add(Box.createHorizontalGlue());
        add(leave);
        
        refresh();
    }

    /**
     * specify the content of the Panel, according user's actions menu
     */
    public void actionPerformed(ActionEvent actionEvent) {
        String command = actionEvent.getActionCommand();
  
        switch (command) {
            case "random25":
                Board random25 = new Board(25, 25, false); // false = board isn't empty, cf board constructor
                frame.setContentPane(new Panel(random25));
                updateUI();
                break;
            case "random50":
                Board random50 = new Board(50, 50, false);
                frame.setContentPane(new Panel(random50));
                updateUI();
                break;
            case "random75":
                Board random75 = new Board(75, 75, false);
                frame.setContentPane(new Panel(random75));
                updateUI();
                break;
            case "random":
                int lengthRandom = Integer.parseInt(JOptionPane.showInputDialog(frame, Data.sLENGTH));
                int heightRandom = Integer.parseInt(JOptionPane.showInputDialog(frame, Data.sHEIGHT));
                if(lengthRandom > 0 && heightRandom > 0 && lengthRandom <= 100 && heightRandom <= 100) {
                    Board random = new Board(heightRandom, lengthRandom, false);
                    frame.setContentPane(new Panel(random));
                    updateUI();
                } else {
                    JOptionPane.showMessageDialog(frame, Data.sSIZE_ERROR, Data.sERROR, JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "empty25":
                Board empty25 = new Board(25, 25, true);
                frame.setContentPane(new Panel(empty25));
                updateUI();
                break;
            case "empty50":
                Board empty50 = new Board(50, 50, true);
                frame.setContentPane(new Panel(empty50));
                updateUI();
                break;
            case "empty75":
                Board empty75 = new Board(75, 75, true);
                frame.setContentPane(new Panel(empty75));
                updateUI();
                break;
            case "empty":
                int lengthEmpty = Integer.parseInt(JOptionPane.showInputDialog(frame, Data.sLENGTH));
                int heightEmpty = Integer.parseInt(JOptionPane.showInputDialog(frame, Data.sHEIGHT));
                if(lengthEmpty > 0 && heightEmpty > 0 && lengthEmpty <= 100 && heightEmpty <= 100) {
                    Board random = new Board(heightEmpty, lengthEmpty, true);
                    frame.setContentPane(new Panel(random));
                    updateUI();
                } else {
                    JOptionPane.showMessageDialog(frame, Data.sSIZE_ERROR, Data.sERROR, JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "f5":
            	
            		refresh();
            	
            	break;
            case "leave":
            	int retour = JOptionPane.showConfirmDialog(null, Data.sLEAVE + " ?", Data.sCONFIRMATION ,JOptionPane.OK_CANCEL_OPTION);
    			if (retour == JOptionPane.OK_OPTION)
    				System.exit(0);
    			else
    				break;
            default:
            	frame.setContentPane(new Panel((Board)ReadWriteBoard.readBoard(new File(command))));
            	updateUI();
            	break;
        }

        
    }
    
    /**
     * refreshes the menu to display all saved boards
     */
    public void refresh(){
    	 menuSaved.removeAll();
    	 File directory = new File("files");
         File[] files=directory.listFiles();
         for(int i = 0; i < files.length ; i++){
        	 String nom = files[i].toString();
        	 nom=nom.substring(6,nom.length()-4);//cut the path and the extenstion, the name of the directory must be files !
         	 JMenuItem expBoard = new JMenuItem(nom);
         	 expBoard.setActionCommand(files[i].getPath());
         	 expBoard.addActionListener(this);
         	 menuSaved.add(expBoard);
         }
             JMenuItem refresh = new JMenuItem(Data.sREFRESH, new ImageIcon("f5i.png"));
             refresh.setActionCommand("f5");
             refresh.addActionListener(this);
             menuSaved.addSeparator();
             menuSaved.add(refresh);
        	updateUI();
         
    }
    
}
