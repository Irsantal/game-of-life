package view;

import model.Cell;

import javax.swing.*;

import java.awt.*;

/**
 * The class ButtonCell represents a JButton which contains a Cell
 * @author Irvinn Rochette - Cyril Dubos
 */
public class ButtonCell extends JButton {

    /**
     *the Cell related to the JButton
     */
    private Cell cell;

    /**
     * creates a new Button 
     * @param cell the cell related to this button
     */
    public ButtonCell(Cell cell) {
        this.cell = cell;
        update();
    }

    /**
     * Update this ButtonCell, depending the state
     */
    public void update() {
        if(cell.getState()) {
            setBackground(Color.BLACK);
        } else {
            setBackground(Color.WHITE);
        }
    }

	/**
	 * @return the cell
	 */
	public Cell getCell() {
		return cell;
	}
    
    
}