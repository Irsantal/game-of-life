package view;

import controller.Controller;
import model.Board;

import javax.swing.*;
import java.awt.*;

/**
 * The class Panel represents the main JPanel
 * @author Irvinn Rochette - Cyril Dubos
 */
public class Panel extends JPanel {

    /**
     * The JPanel which specifiy the population and the generation
     */
    private PanelInformations panelInformations;

    /**
     * The JPanel which contains a Board
     */
    private PanelBoard panelBoard;

    /**
     * The JPanel with all actions for the user
     */
    private PanelActions panelActions = new PanelActions();


    /**
     * creates a new panel whith the 3 subpanels of the application 
     * @param board the board game spcified in the menu
     */
    public Panel(Board board) {
        setLayout(new BorderLayout());

        panelBoard = new PanelBoard(board);
        panelInformations = new PanelInformations(board);
        Controller crtl = new Controller(panelInformations, panelBoard, panelActions);
        crtl.start();
        add(panelInformations, BorderLayout.NORTH);
        add(panelBoard, BorderLayout.CENTER);
        add(panelActions, BorderLayout.SOUTH);

    }
}