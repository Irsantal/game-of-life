package view;

import model.Board;
import javax.swing.*;

import controller.Controller;

import java.awt.*;


/**
 * The class PanelBoard represents a JPanel which contains a Board
 * @author Irvinn Rochette - Cyril Dubos
 */
public class PanelBoard extends JPanel {

    /**
     * the game board
     */
    private Board board;

    /**
     * The collection of ButtonCell which contained in the Board
     */
    private ButtonCell[][] buttonsCell;

    /**
     * creates the displayed board, which is ButtonsCells related to each cell of the board
     * @param board the board created
     */
    public PanelBoard(Board board) {
        this.board = board;

        setLayout(new GridLayout(board.getLength(), board.getHeight()));
        buttonsCell = new ButtonCell[board.getLength()][board.getHeight()];

        for (int i = 0; i < board.getLength(); i++) {
            for (int j = 0; j < board.getHeight(); j++) {
                buttonsCell[i][j] = new ButtonCell(board.getCells()[i][j]);
                buttonsCell[i][j].setToolTipText(""+ board.neighbours(i, j));
                buttonsCell[i][j].setActionCommand(i+ " x "+j);
                add(buttonsCell[i][j]);
            }
        }
    }

    /**
     * Update all ButtonCell of the Board in this JPanel
     */
    public void update() {
        for (int i = 0; i < board.getLength(); i++) {
            for (int j = 0; j < board.getHeight(); j++) {
                buttonsCell[i][j].update();
                buttonsCell[i][j].setToolTipText(""+ board.neighbours(i, j));
            }
        }
    }
    /**
     * use to listen buttons and manages actions in the contorller
     * @param parC the controller of the application
     */
    public void listenRecorder (Controller parC) {
    	for (int i = 0; i < board.getLength(); i++) {
            for (int j = 0; j < board.getHeight(); j++)
                buttonsCell[i][j].addActionListener(parC);
        }
    }

	/**
	 * @return the buttonsCell
	 */
	public ButtonCell[][] getButtonsCell() {
		return buttonsCell;
	}

	/**
	 * @return the board
	 */
	public Board getBoard() {
		return board;
	}
    
    
}