package view;

import javax.swing.*;

import controller.Controller;
import model.Data;

/**
 * The class PanelAction represents a JPanel which contains the JButtons of different actions
 * @author Irvinn Rochette - Cyril Dubos
 */
public class PanelActions extends JPanel {

    /**
     * The JButton which used to pause the game
     */
    private JButton buttonPause = new JButton(Data.sPAUSE);

    /**
     * The JButton which used to fast forward the game
     */
    private JButton buttonFastForward = new JButton(Data.sFAST_FORWARD);

    /**
     * The JButton which used to display the next generation
     */
    private JButton buttonNextGeneration = new JButton(Data.sNEXT_GENERATION);

    /**
     * the name of the save if user want to save
     */
    private JTextField textFieldName = new JTextField(10);

    /**
     * The JButton which used to save the Board
     */
    private JButton buttonSave = new JButton(Data.sSAVE);

    /**
     * creates a new panel with all buttons
     */
    public PanelActions() {
    	
    	buttonPause.setActionCommand("bP");
        buttonPause.setMnemonic(Data.cPAUSE);
    	buttonFastForward.setActionCommand("bFF");
        buttonFastForward.setMnemonic(Data.cFAST_FORWARD);
    	buttonNextGeneration.setActionCommand("bNextGen");
        buttonNextGeneration.setMnemonic(Data.cNEXT_GENERATION);
    	buttonSave.setActionCommand("bS");
        buttonSave.setMnemonic(Data.cSAVE);
    	
        add(buttonPause);
        add(buttonFastForward);
        add(buttonNextGeneration);
        JLabel labelBoardName = new JLabel(Data.sBOARD_NAME);
        labelBoardName.setLabelFor(textFieldName);
        labelBoardName.setDisplayedMnemonic('N');
        add(labelBoardName);
        add(textFieldName);
        add(buttonSave);
    }
    
    /**
     * use to listen buttons and manages actions in the contorller
     * @param parC the controller of the application
     */
    public void listenRecorder(Controller parC) {
    	buttonPause.addActionListener(parC);
    	buttonFastForward.addActionListener(parC);
    	buttonNextGeneration.addActionListener(parC);
    	buttonSave.addActionListener(parC);
    }

	/**
	 * @return the textFieldName
	 */
	public JTextField getTextFieldName() {
		return textFieldName;
	}

	/**
	 * @param textFieldName the textFieldName to set
	 */
	public void setTextFieldName(JTextField textFieldName) {
		this.textFieldName = textFieldName;
	}
    
    
}