package view;

import model.Data;

import javax.swing.*;

/**
 * The class Frame represents the main JFrame
 * @author Irvinn Rochette - Cyril Dubos
 */
public class Frame extends JFrame {

    /**
     * creates a new frame
     */
    public Frame() {
        super(Data.sGAME_OF_LIFE);

        //panel = new Panel(board);
        setContentPane(new JLabel(Data.sWELCOME_MESSAGE, JLabel.CENTER));
        //setContentPane(panel);
        setJMenuBar(new MenuBar(this));

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(720, 720);
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    /**
     * launch the application
     * @param args /
     */
    public static void main(String[] args) {
        new Frame();
    }
}